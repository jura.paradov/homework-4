class Scope:

    """Scope - представляет доступ к значениям по именам
    (к функциям и именованным константам).
    Scope может иметь родителя, и если поиск по имени
    в текущем Scope не успешен, то если у Scope есть родитель,
    то поиск делегируется родителю.
    Scope должен поддерживать dict-like интерфейс доступа
    (см. на специальные функции __getitem__ и __setitem__)
    """

    def __init__(self, parent=None):
        self.values = {}
        self.parent = parent

    def __setitem__(self, key, value):
        self.values[key] = value

    def __getitem__(self, key):
        if key in self.values:
            return self.values[key]
        else:
            return self.parent[key]

class Number:

    """Number - представляет число в программе.
    Все числа в нашем языке целые."""

    def __init__(self, value):
        self.value = int(value)

    def evaluate(self, _):
        return self


class Function:

    """Function - представляет функцию в программе.
    Функция - второй тип поддерживаемый языком.
    Функции можно передавать в другие функции,
    и возвращать из функций.
    Функция состоит из тела и списка имен аргументов.
    Тело функции это список выражений,
    т. е.  у каждого из них есть метод evaluate.
    Во время вычисления функции (метод evaluate),
    все объекты тела функции вычисляются последовательно,
    и результат вычисления последнего из них
    является результатом вычисления функции.
    Список имен аргументов - список имен
    формальных параметров функции."""

    def __init__(self, args, body):
        self.args = args
        self.body = body

    def evaluate(self, scope):
        result = None
        for x in self.body:
            result = x.evaluate(scope)
        return result


class FunctionDefinition:

    """FunctionDefinition - представляет определение функции,
    т. е. связывает некоторое имя с объектом Function.
    Результатом вычисления FunctionDefinition является
    обновление текущего Scope - в него
    добавляется новое значение типа Function."""

    def __init__(self, name, function):
        self.name = name
        self.function = function

    def evaluate(self, scope):
        scope[self.name] = self.function
        return self.function


class Conditional:

    """
    Conditional - представляет ветвление в программе, т. е. if.
    """

    def __init__(self, condition, if_true, if_false=None):
        self.condition = condition
        self.true = if_true
        self.false = if_false

    def evaluate(self, scope):
        result = self.condition.evaluate(scope)
        body = self.true if result.value != 0 else self.false
        if body is not None:
            for x in body:
                result = x.evaluate(scope)
        return result


class Print:

    """Print - печатает значение выражения на отдельной строке."""

    def __init__(self, expr):
        self.expr = expr

    def evaluate(self, scope):
        result = self.expr.evaluate(scope)
        print(result.value)
        return result


class Read:

    """Read - читает число из стандартного потока ввода
     и обновляет текущий Scope.
     Каждое входное число располагается на отдельной строке
     (никаких пустых строк и лишних символов не будет).
     """

    def __init__(self, name):
        self.name = name

    def evaluate(self, scope):
        result = Number(int(input()))
        scope[self.name] = result
        return result


class FunctionCall:

    """
    FunctionCall - представляет вызов функции в программе.
    В результате вызова функции должен создаваться новый объект Scope,
    являющий дочерним для текущего Scope
    (т. е. текущий Scope должен стать для него родителем).
    Новый Scope станет текущим Scope-ом при вычислении тела функции.
    """

    def __init__(self, fun_expr, args):
        self.fun_expr = fun_expr
        self.args = args

    def evaluate(self, scope):
        function = self.fun_expr.evaluate(scope)
        call_scope = Scope(scope)
        for arg_name, arg in zip(function.args, self.args):
            call_scope[arg_name] = arg.evaluate(scope)
        return function.evaluate(call_scope)


class Reference:

    """Reference - получение объекта
    (функции или переменной) по его имени."""

    def __init__(self, name):
        self.name = name

    def evaluate(self, scope):
        return scope[self.name]


class BinaryOperation:

    """BinaryOperation - представляет бинарную операцию над двумя выражениями.
    Результатом вычисления бинарной операции является объект Number.
    Поддерживаемые операции:
    “+”, “-”, “*”, “/”, “%”, “==”, “!=”,
    “<”, “>”, “<=”, “>=”, “&&”, “||”."""

    def __init__(self, lhs, op, rhs):
        self.op = op
        self.lhs = lhs
        self.rhs = rhs

    def evaluate(self, scope):
        funcs = {
            '+': lambda x, y: x + y,
            '-': lambda x, y: x - y,
            '*': lambda x, y: x * y,
            '/': lambda x, y: x / y,
            '%': lambda x, y: x % y,
            '==': lambda x, y: x == y,
            '!=': lambda x, y: x != y,
            '<': lambda x, y: x < y,
            '>': lambda x, y: x > y,
            '<=': lambda x, y: x <= y,
            '>=': lambda x, y: x >= y,
            '&&': lambda x, y: x and y,
            '||': lambda x, y: x or y,
        }

        return Number(funcs[self.op](self.lhs.evaluate(scope).value, self.rhs.evaluate(scope).value))


class UnaryOperation:

    """UnaryOperation - представляет унарную операцию над выражением.
    Результатом вычисления унарной операции является объект Number.
    Поддерживаемые операции: “-”, “!”."""

    def __init__(self, op, expr):
        self.op = op
        self.expr = expr

    def evaluate(self, scope):
        if self.op == "!":
            result = Number(not self.expr.evaluate(scope).value)
        else:
            result = Number(- self.expr.evaluate(scope).value)
        return result


def example():
    parent = Scope()
    parent["foo"] = Function(('hello', 'world'),
                             [Print(BinaryOperation(Reference('hello'),
                                                    '+',
                                                    Reference('world')))])
    parent["bar"] = Number(10)
    scope = Scope(parent)
    assert 10 == scope["bar"].value
    scope["bar"] = Number(20)
    assert scope["bar"].value == 20
    print('It should print 2: ', end=' ')
    FunctionCall(FunctionDefinition('foo', parent['foo']),
                 [Number(5), UnaryOperation('-', Number(3))]).evaluate(scope)


def scopes():
    parent = Scope()
    parent["ten"] = Number(10)
    child = Scope(parent)
    grandchild = Scope(child)
    child["ten"] = Number(42)
    assert 10 == Reference("ten").evaluate(parent).value
    assert 42 == Reference("ten").evaluate(child).value
    assert 42 == Reference("ten").evaluate(grandchild).value
    print("Test 'scopes' checked")


def conditions():
    parent = Scope()
    parent["ten"] = Number(10)
    parent["if"] = Function(["a"],
                            [Conditional(BinaryOperation(Reference("ten"),
                                                         '!=',
                                                         Reference("a")),
                                         [Number(-1)],
                                         [Number(1337)])])

    parent["false_result"] = FunctionCall(FunctionDefinition("if", parent["if"]),
                                          [Reference("ten")]).evaluate(parent)
    assert 1337 == parent["false_result"].value

    parent["not_ten"] = Number(134)
    parent["true_result"] = FunctionCall(Reference("if"),
                                         [Reference("not_ten")]).evaluate(parent)
    assert -1 == parent["true_result"].value
    print("Test 'conditions' checked")


def ops15():
    parent = Scope()
    parent["97"] = Number(97)
    parent["83"] = Number(83)
    parent["result"] = BinaryOperation(Reference("97"), '+', Reference('83')).evaluate(parent)
    assert 180 == Reference("result").evaluate(parent).value

    parent["result"] = BinaryOperation(Reference("97"), '-', Reference('83')).evaluate(parent)
    assert 14 == Reference("result").evaluate(parent).value

    parent["result"] = BinaryOperation(Reference("97"), '*', Reference('83')).evaluate(parent)
    assert 8051 == Reference("result").evaluate(parent).value

    parent["result"] = BinaryOperation(Reference("97"), '/', Reference('83')).evaluate(parent)
    assert 1 == Reference("result").evaluate(parent).value

    parent["result"] = BinaryOperation(Reference("97"), '%', Reference('83')).evaluate(parent)
    assert 14 == Reference("result").evaluate(parent).value

    parent["result"] = BinaryOperation(Reference("97"), '==', Reference('83')).evaluate(parent)
    assert 0 == Reference("result").evaluate(parent).value

    parent["result"] = BinaryOperation(Reference("97"), '!=', Reference('83')).evaluate(parent)
    assert 1 == Reference("result").evaluate(parent).value

    parent["result"] = BinaryOperation(Reference("97"), '<', Reference('83')).evaluate(parent)
    assert 0 == Reference("result").evaluate(parent).value

    parent["result"] = BinaryOperation(Reference("97"), '>', Reference('83')).evaluate(parent)
    assert 1 == Reference("result").evaluate(parent).value

    parent["result"] = BinaryOperation(Reference("97"), '<=', Reference('83')).evaluate(parent)
    assert 0 == Reference("result").evaluate(parent).value

    parent["result"] = BinaryOperation(Reference("97"), '>=', Reference('83')).evaluate(parent)
    assert 1 == Reference("result").evaluate(parent).value

    parent["result"] = BinaryOperation(Reference("97"), '&&', Reference('83')).evaluate(parent)
    assert (97 and 83) == Reference("result").evaluate(parent).value

    parent["result"] = BinaryOperation(Reference("97"), '||', Reference('83')).evaluate(parent)
    assert (97 or 83) == Reference("result").evaluate(parent).value

    parent["result"] = UnaryOperation('-', Reference("97")).evaluate(parent)
    assert -97 == Reference("result").evaluate(parent).value

    parent["result"] = UnaryOperation('!', Reference("83")).evaluate(parent)
    assert (not 83) == Reference("result").evaluate(parent).value

    print("Test 'ops15' checked")


def func():
    parent = Scope()
    FunctionDefinition("foo",
                       Function(['a', 'b'],
                                [Print(Reference('b')),
                                 Print(Reference('a'))])).evaluate(parent)
    print("Enter first number:", end=' ')
    parent["result"] = Read("num1").evaluate(parent)
    assert Reference('num1').evaluate(parent).value == parent['result'].evaluate(parent).value
    print("Enter second number:", end=' ')
    Read("num2").evaluate(parent)
    print("They should be printed in swapped order: ")
    parent['result'] = FunctionCall(Reference("foo"),
                                    [Reference('num1'), Reference('num2')]).evaluate(parent)
    assert Reference('num1').evaluate(parent).value == parent['result'].evaluate(parent).value
    print("Test 'func' checked")


if __name__ == '__main__':
    example()
    scopes()
    conditions()
    ops15()
    func()
